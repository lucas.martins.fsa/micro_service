# Aplicação para Votar e visualizar resultados

Projeto para preparar um sistema de votação utilizando um servidor Banco de Dados Postgres e Redis em Ambiente do Kubernetes.

- no projeto está organizado os manifestos do Kubernetes em cada uma das Pastas;

- Diretorio Deployments
   - db.yml
   - redis.yml
   - result.yml
   - vote.yml
   - worker.yml

- Diretorio namespace : definindo o namespace do projeto
   - vote.yml

- Diretorio Service: Definindo os serviços correspondentes de cada aplicação

   - db.yml
   - redis.yml
   - result.yml
   - vote.yml
